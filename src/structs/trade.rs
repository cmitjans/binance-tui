pub struct Trade {
    pub price: f32,
    pub quantity: f32,
    pub buyer_market: bool,
    pub timestamp: u64,
}
