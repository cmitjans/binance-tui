pub struct KLine {
    pub high: f32,
    pub low: f32,
    pub open: f32,
    pub close: f32,
}
