use std::usize;

use chrono::NaiveDateTime;
use tui::{
    style::{Color, Style},
    text::{Span, Spans, Text},
    widgets::{Block, Borders, List, ListItem, Widget},
};

use crate::app::App;

pub struct Trades<'a> {
    app: &'a App,
}

impl<'a> Trades<'a> {
    pub fn new(app: &'a App) -> Self {
        Trades { app }
    }
}

impl Widget for Trades<'_> {
    fn render(self, area: tui::layout::Rect, buf: &mut tui::buffer::Buffer) {
        let block = Block::default().borders(Borders::ALL).title("Trades");
        let inner_area = block.inner(area);
        block.render(area, buf);

        let trades = self.app.truncate_and_get_trades(inner_area.height.into());

        let list_items: Vec<ListItem> = trades
            .iter()
            .map(|trade| {
                let seconds = (trade.timestamp / 1000) as i64;
                let miliseconds = trade.timestamp % 1000;
                let nanoseconds = (miliseconds * 1000000) as u32;
                let time = NaiveDateTime::from_timestamp_opt(seconds, nanoseconds).unwrap();

                let padding =
                    format!("{:.2}", trade.price).len() + time.format("%H:%M:%S").to_string().len();

                if padding >= inner_area.width.into() {
                    return ListItem::new("No space");
                }

                let price_text = Span::styled(
                    format!("{:.2}", trade.price),
                    Style::default().fg(if trade.buyer_market {
                        Color::Green
                    } else {
                        Color::Red
                    }),
                );
                let quantity_text = Span::raw(format!(
                    "{:^1$.3}",
                    trade.quantity,
                    inner_area.width as usize - padding
                ));
                let time_text = Span::raw(time.format("%H:%M:%S").to_string());

                ListItem::new(Text::from(Spans::from(vec![
                    price_text,
                    quantity_text,
                    time_text,
                ])))
            })
            .collect();

        let list = List::new(list_items);

        list.render(inner_area, buf);
    }
}
