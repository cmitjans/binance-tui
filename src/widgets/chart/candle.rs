use tui::{
    buffer::Buffer,
    layout::Rect,
    style::{Color, Style},
    symbols::{block::ONE_EIGHTH, line::VERTICAL},
    widgets::Widget,
};

pub struct Candle {
    high: f32,
    low: f32,
    open: f32,
    close: f32,
}

impl Candle {
    pub fn new(high: f32, low: f32, open: f32, close: f32) -> Self {
        Candle {
            high,
            low,
            open,
            close,
        }
    }
}

impl Candle {
    fn get_color(&self) -> Color {
        if self.open < self.close {
            Color::Green
        } else {
            Color::Red
        }
    }
}

impl Widget for Candle {
    fn render(self, area: Rect, buf: &mut Buffer) {
        let (top_price, bottom_price) = match self.open < self.close {
            true => (self.close, self.open),
            false => (self.open, self.close),
        };

        let mid_x = area.x + area.width / 2;

        let steps = area.height as f32;

        let high_step = area.y + area.height - (self.high * steps).round() as u16;
        let low_step = area.y + area.height - (self.low * steps).round() as u16;
        let top_step = area.y + area.height - (top_price * steps).round() as u16;
        let bottom_step = area.y + area.height - (bottom_price * steps).round() as u16;

        buf.set_style(
            Rect::new(area.x, top_step, area.width, bottom_step - top_step),
            Style::default().bg(self.get_color()),
        );

        (high_step..low_step).for_each(|y| {
            buf.get_mut(mid_x, y)
                .set_symbol(if area.width % 2 == 0 {
                    ONE_EIGHTH
                } else {
                    VERTICAL
                })
                .set_fg(self.get_color());
        });
    }
}
