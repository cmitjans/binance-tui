mod candle;

use candle::Candle;
use tui::{
    layout::{Alignment, Margin, Rect},
    widgets::{Block, Borders, Clear, Paragraph, Widget},
};

use crate::app::App;

pub struct Chart<'a> {
    app: &'a App,
}

impl<'a> Chart<'_> {
    pub fn new(app: &'a App) -> Chart<'a> {
        Chart { app }
    }
}

impl Widget for Chart<'_> {
    fn render(self, area: tui::layout::Rect, buf: &mut tui::buffer::Buffer) {
        let interval = self.app.get_interval();
        let block = Block::default()
            .borders(Borders::ALL)
            .title(interval.as_str());

        let inner_area = block.inner(area);
        block.render(area, buf);

        let max_klines = inner_area.width / 2;
        let mut klines = self.app.get_klines(max_klines);

        let mut number_of_displayed_klines =
            (self.app.get_chart_zoom() * max_klines as f32).floor() as usize;

        if number_of_displayed_klines < 4 && klines.len() > 3 {
            number_of_displayed_klines = 4;
        }

        if number_of_displayed_klines <= klines.len() {
            klines.truncate(number_of_displayed_klines)
        }

        if klines.len() == 0 {
            Paragraph::new("Loading...")
                .alignment(Alignment::Center)
                .render(inner_area, buf);
            return;
        }

        let (max_price_index, max_price) = klines
            .iter()
            .enumerate()
            .max_by(|x, y| x.1.high.partial_cmp(&y.1.high).unwrap())
            .map(|(index, kline)| (index, kline.high))
            .unwrap();

        let (min_price_index, min_price) = klines
            .iter()
            .enumerate()
            .min_by(|x, y| x.1.low.partial_cmp(&y.1.low).unwrap())
            .map(|(index, kline)| (index, kline.low))
            .unwrap();

        let max_interval = max_price - min_price;

        let width_per_candle = inner_area.width / number_of_displayed_klines as u16;

        for (candle_number, kline) in klines.iter().enumerate() {
            let candle = Candle::new(
                (kline.high - min_price) / max_interval,
                (kline.low - min_price) / max_interval,
                (kline.open - min_price) / max_interval,
                (kline.close - min_price) / max_interval,
            );
            let candle_area = Rect::new(
                inner_area.x
                    + (inner_area.width
                        - (candle_number as u16 * width_per_candle)
                        - width_per_candle),
                inner_area.y,
                width_per_candle,
                inner_area.height,
            )
            .inner(&Margin {
                horizontal: 1,
                vertical: 0,
            });

            candle.render(candle_area, buf);
        }

        /*
         * MAX PRICE TEXT
         */

        let max_price_text = format!("{}", max_price);
        let mut max_price_rect_x = inner_area.x
            + (inner_area.width - (max_price_index as u16 * width_per_candle) - width_per_candle)
            + width_per_candle / 2
            + 1;

        if max_price_rect_x + max_price_text.len() as u16 > inner_area.x + inner_area.width {
            max_price_rect_x -= max_price_text.len() as u16;
            max_price_rect_x -= 2;
        }

        let max_price_rect = Rect::new(
            max_price_rect_x,
            inner_area.y,
            max_price_text.len() as u16,
            1,
        );
        Clear {}.render(max_price_rect, buf);
        Paragraph::new(format!("{}", max_price)).render(max_price_rect, buf);

        /*
         * MIN PRICE TEXT
         */

        let min_price_text = format!("{}", min_price);
        let mut min_price_rect_x = inner_area.x
            + (inner_area.width - (min_price_index as u16 * width_per_candle) - width_per_candle)
            + width_per_candle / 2
            + 1;

        if min_price_rect_x + min_price_text.len() as u16 > inner_area.x + inner_area.width {
            min_price_rect_x -= min_price_text.len() as u16;
            min_price_rect_x -= 2;
        }
        let min_price_text = format!("{}", max_price);
        let min_price_rect = Rect::new(
            min_price_rect_x,
            inner_area.y + inner_area.height - 1,
            min_price_text.len() as u16,
            1,
        );
        Clear {}.render(min_price_rect, buf);
        Paragraph::new(format!("{}", min_price)).render(min_price_rect, buf);
    }
}
