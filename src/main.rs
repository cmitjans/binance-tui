mod app;
mod events;
mod structs;
mod widgets;

use app::App;
use binance::{enums::KLineInterval, futures::FuturesClient};
use events::{Event, Events};
use std::{error::Error, io::stdout};
use termion::{
    event::{Event as InputEvent, Key, MouseButton, MouseEvent},
    input::MouseTerminal,
    raw::IntoRawMode,
    screen::AlternateScreen,
};
use tui::{
    backend::TermionBackend,
    layout::{Constraint, Direction, Layout},
    Terminal,
};
use widgets::{Chart, Trades};

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    let stdout = stdout().into_raw_mode()?;
    let stdout = MouseTerminal::from(stdout);
    let stdout = AlternateScreen::from(stdout);
    let backend = TermionBackend::new(stdout);
    let mut terminal = Terminal::new(backend)?;

    let events = Events::new();

    let futures_client = FuturesClient::new();
    let mut app = App::new(futures_client);

    loop {
        terminal.draw(|f| {
            let chunks = Layout::default()
                .direction(Direction::Horizontal)
                .margin(2)
                .constraints([Constraint::Min(30), Constraint::Length(31)].as_ref())
                .split(f.size());

            f.render_widget(Chart::new(&app), chunks[0]);
            f.render_widget(Trades::new(&app), chunks[1]);
        })?;

        match events.next()? {
            Event::Input(event) => match event {
                InputEvent::Key(key) => match key {
                    Key::Char('q') => break,
                    Key::Char('1') => app.set_interval(KLineInterval::Minute),
                    Key::Char('3') => app.set_interval(KLineInterval::ThreeMinutes),
                    Key::Char('5') => app.set_interval(KLineInterval::FiveMinutes),
                    Key::Char('8') => app.set_interval(KLineInterval::Hour),
                    Key::Char('9') => app.set_interval(KLineInterval::Day),
                    _ => (),
                },
                InputEvent::Mouse(mouse_event) => match mouse_event {
                    MouseEvent::Press(press_event, _, _) => match press_event {
                        MouseButton::WheelUp => app.zoom_out(),
                        MouseButton::WheelDown => app.zoom_in(),
                        _ => (),
                    },
                    _ => (),
                },
                _ => (),
            },
            _ => (),
        }
    }

    Ok(())
}
