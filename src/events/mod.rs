mod event;

pub use event::Event;

use std::sync::mpsc;
use std::thread;
use std::{io, time::Duration};

use termion::event::Event as InputEvent;
use termion::input::TermRead;

/// A small event handler that wrap termion input and tick events. Each event
/// type is handled in its own thread and returned to a common `Receiver`
pub struct Events {
    rx: mpsc::Receiver<Event<InputEvent>>,
}

impl Events {
    pub fn new() -> Events {
        let (tx, rx) = mpsc::channel();

        let key_tx = tx.clone();
        thread::spawn(move || {
            let stdin = io::stdin();
            for evt in stdin.events() {
                if let Ok(event) = evt {
                    if let Err(err) = key_tx.send(Event::Input(event)) {
                        eprintln!("{}", err);
                        return;
                    }
                }
            }
        });

        thread::spawn(move || loop {
            if tx.send(Event::Tick).is_err() {
                break;
            }
            thread::sleep(Duration::from_millis(100));
        });

        Events { rx }
    }

    pub fn next(&self) -> Result<Event<InputEvent>, mpsc::RecvError> {
        self.rx.recv()
    }
}
