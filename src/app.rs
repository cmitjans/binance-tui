use std::{
    collections::HashMap,
    sync::{Arc, Mutex},
    thread,
};

use binance::{
    enums::KLineInterval,
    futures::{
        websockets::{AggregateTradeEvent, Stream, WebsocketEvent},
        FuturesClient,
    },
};

use crate::structs::{KLine, Trade};

type KLinesHash = HashMap<KLineInterval, HashMap<u64, KLine>>;

pub struct App {
    futures_client: Arc<FuturesClient>,
    klines: Arc<Mutex<KLinesHash>>,
    trades: Arc<Mutex<Vec<AggregateTradeEvent>>>,
    interval: KLineInterval,
    klines_stream: Stream,
    chart_zoom: f32,
}

impl App {
    pub fn new(futures_client: FuturesClient) -> Self {
        let interval = KLineInterval::Minute;
        let klines_stream = futures_client.kline_stream("btcusdt", interval);
        futures_client.aggregate_trade_stream("btcusdt");

        let klines: Arc<Mutex<KLinesHash>> = Arc::new(Mutex::new(HashMap::new()));
        let trades = Arc::new(Mutex::new(vec![]));

        let cloned_klines = Arc::clone(&klines);
        let cloned_trades = Arc::clone(&trades);

        futures_client.read(move |event| match event {
            WebsocketEvent::Kline(kline) => match cloned_klines.try_lock() {
                Ok(mut locked_klines) => {
                    let new_kline = KLine {
                        high: kline.get_kline_data().get_high_price(),
                        low: kline.get_kline_data().get_low_price(),
                        open: kline.get_kline_data().get_open_price(),
                        close: kline.get_kline_data().get_close_price(),
                    };
                    let interval_hash = locked_klines
                        .entry(kline.get_kline_data().get_interval())
                        .or_insert(HashMap::new());

                    interval_hash.insert(kline.get_kline_data().get_open_time(), new_kline);
                }
                _ => (),
            },
            WebsocketEvent::AggregateTrade(trade) => {
                cloned_trades.lock().unwrap().insert(0, trade);
            }
            _ => (),
        });

        App {
            futures_client: Arc::new(futures_client),
            klines,
            trades,
            interval,
            klines_stream,
            chart_zoom: 0.5,
        }
    }

    pub fn truncate_and_get_trades(&self, length: usize) -> Vec<Trade> {
        let mut locked_trades = self.trades.lock().unwrap();

        locked_trades.truncate(length);

        locked_trades
            .iter()
            .map(|trade| Trade {
                price: trade.get_price(),
                quantity: trade.get_quantity(),
                buyer_market: trade.is_market_maker(),
                timestamp: trade.get_trade_timestamp(),
            })
            .collect()
    }

    pub fn set_interval(&mut self, interval: KLineInterval) {
        self.interval = interval;

        self.futures_client.unsubscribe(&self.klines_stream);

        self.klines_stream = self.futures_client.kline_stream("btcusdt", interval);
    }

    pub fn get_interval(&self) -> KLineInterval {
        self.interval
    }

    pub fn zoom_in(&mut self) {
        self.chart_zoom = f32::min(1.0, self.chart_zoom + 0.05);
    }

    pub fn zoom_out(&mut self) {
        self.chart_zoom = f32::max(0.0, self.chart_zoom - 0.05);
    }

    pub fn get_chart_zoom(&self) -> f32 {
        self.chart_zoom
    }

    pub fn get_klines(&self, max_number_of_klines: u16) -> Vec<KLine> {
        let mut intervals = self.klines.lock().unwrap();
        let klines = intervals.entry(self.interval).or_insert(HashMap::new());

        let mut klines_vec = klines.iter().collect::<Vec<_>>();
        klines_vec.sort_by(|a, b| b.0.cmp(a.0));

        if klines.len() < max_number_of_klines.into() {
            self.update_klines(max_number_of_klines);
        } else {
            klines_vec.truncate(max_number_of_klines.into());
        }

        klines_vec
            .iter()
            .map(|(_, kline)| KLine {
                open: kline.open,
                close: kline.close,
                high: kline.high,
                low: kline.low,
            })
            .collect()
    }
}

impl App {
    fn update_klines(&self, max_number_of_klines: u16) {
        let cloned_interval = self.interval.clone();
        let cloned_klines = Arc::clone(&self.klines);
        let cloned_futures_client = self.futures_client.clone();

        thread::spawn(move || {
            cloned_futures_client
                .get_kline(
                    "btcusdt",
                    cloned_interval,
                    None,
                    None,
                    Some(max_number_of_klines),
                )
                .unwrap()
                .iter()
                .for_each(|kline_data| {
                    let mut locked_klines = cloned_klines.lock().unwrap();
                    let interval_hash = locked_klines
                        .entry(cloned_interval)
                        .or_insert(HashMap::new());
                    interval_hash.insert(
                        kline_data.get_open_time(),
                        KLine {
                            high: kline_data.get_high_price(),
                            low: kline_data.get_low_price(),
                            open: kline_data.get_open_price(),
                            close: kline_data.get_close_price(),
                        },
                    );
                });
        });
    }
}
